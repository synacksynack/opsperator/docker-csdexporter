FROM opsperator/java

ARG DO_UPGRADE=
ENV CS=https://github.com/criteo/cassandra_exporter/releases/download/ \
    XPVERSION=2.3.5

# Prometheus Cassandra Exporter image for OpenShift Origin

LABEL io.k8s.description="Prometheus Apache Cassandra Exporter." \
      io.k8s.display-name="Prometheus Cassandra Exporter ${XPVERSION}" \
      io.openshift.expose-services="9113:exporter" \
      io.openshift.tags="cassandra,exporter,prometheus" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-csdexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="${XPVERSION}"

USER root

COPY config/* /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	apt-get upgrade -y; \
	apt-get dist-upgrade -y; \
    fi \
    && echo "# Install Exporter" \
    && apt-get install -y --no-install-recommends netcat wget \
    && mkdir -p /etc/cassandra_exporter /opt/cassandra_exporter \
    && wget --no-check-certificate \
	"$CS/$XPVERSION/cassandra_exporter-$XPVERSION.jar" \
	-O /opt/cassandra_exporter/cassandra_exporter.jar \
    && mv /config.yml /etc/cassandra_exporter/ \
    && apt-get remove -y --purge wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT ["dumb-init","--","/run-exporter.sh"]
